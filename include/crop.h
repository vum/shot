#ifndef CROP_H
#define CROP_H

#include <X11/extensions/Xfixes.h>
#include <X11/cursorfont.h>
#include <X11/Xutil.h>
#include <X11/Xlib.h>
#include <stdio.h>

#include "globals.h"


// I have to either find a way to separate X11 from Wayland crop or
// find if I can do all of this with GTK so I can put all in screenshot.c

GdkRectangle crop();

#endif
