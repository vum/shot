#ifndef GLOBALS_H
#define GLOBALS_H

#include <gtk/gtk.h>

#define ZOOM_POWER 25
#define DATAPATH "../share/shot/"



struct Widgets {
	GtkImage  *scrot;
	GtkWidget *frame;
	GtkButton *button_upload;
	GtkButton *button_save;
	GtkButton *button_copy;

	GdkPixbuf *screenshot;
	GdkPixbuf *thumbnail;

	GtkFileChooserDialog *save_file;
};

extern struct Widgets widgets;
extern int thumb_zoom;

#endif
