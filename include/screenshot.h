#ifndef SCREENSHOT_H
#define SCREENSHOT_H

#include <gtk/gtk.h>
#include "globals.h"

#define INTERPOLATION GDK_INTERP_HYPER


enum resize_mode {
	MODE_WIDTH, 
	MODE_HEIGHT
};

GdkRectangle resize_keep_aspect(GdkRectangle original, int size, enum resize_mode resize);

GdkPixbuf* take_screenshot(GdkRectangle crop_coords);

GdkPixbuf* get_thumbnail(GdkPixbuf* screenshot);

#endif
