#include "crop.h"


GdkRectangle crop() {
	int rx = 0, ry = 0;
	GdkRectangle rect = {0, 0, 0, 0};
	GdkRectangle crop_coords;
	int done = 0;

	XEvent ev;
	Display *disp = XOpenDisplay(NULL);

	Screen *scr = ScreenOfDisplay(disp, DefaultScreen(disp));
	Window root = RootWindow(disp, XScreenNumberOfScreen(scr));
	Cursor cursor = XCreateFontCursor(disp, XC_tcross);
	XGCValues gcval;
	gcval.foreground = XWhitePixel(disp, 0);
	gcval.function = GXxor;
	gcval.background = XBlackPixel(disp, 0);
	gcval.plane_mask = gcval.background ^ gcval.foreground;
	gcval.subwindow_mode = IncludeInferiors;
	GC gc = XCreateGC(disp, root, GCFunction | GCForeground | GCBackground | GCSubwindowMode, &gcval);
	XColor color;
	XAllocNamedColor(disp, DefaultColormap(disp, DefaultScreen(disp)), "white", &color, &color);
	XGrabPointer(disp, root, False,
		ButtonMotionMask | ButtonPressMask | ButtonReleaseMask,
		GrabModeAsync,
		GrabModeAsync,
		root, cursor, CurrentTime
	);
	XGrabKeyboard(disp, root, False, GrabModeAsync, GrabModeAsync, CurrentTime);

	while (!done) {
		while (XPending(disp)) {
			XNextEvent(disp, &ev);
			switch (ev.type) {
				case MotionNotify:
					//printf("%d\n", ev.xmotion.state & 256);
					if (rect.width) {
						XDrawRectangle(disp, root, gc, rect.x, rect.y, rect.width, rect.height);
						XSetForeground(disp, gc, color.pixel);
					} else {
						XChangeActivePointerGrab(disp, ButtonMotionMask | ButtonReleaseMask, cursor, CurrentTime);
					}
					rect.x = rx;
					rect.y = ry;
					rect.width = ev.xmotion.x - rect.x;
					rect.height = ev.xmotion.y - rect.y;

					if (rect.width < 0) {
						rect.x += rect.width;
						rect.width = 0 - rect.width;
					}
					if (rect.height < 0) {
						rect.y += rect.height;
						rect.height = 0 - rect.height;
					}
					/* draw rectangle */
					XDrawRectangle(disp, root, gc, rect.x, rect.y, rect.width, rect.height);
					XSetForeground(disp, gc, color.pixel);
					XFlush(disp);
					break;
				case ButtonPress:
					if (ev.xbutton.button == 1) {
						rx = ev.xbutton.x;
						ry = ev.xbutton.y;
					}
					break;
				case ButtonRelease:
					done = (ev.xbutton.button == 1);
					break;
				// todo if key escape
			}
		}
	}
	if (rect.width) {
		XDrawRectangle(disp, root, gc, rect.x, rect.y, rect.width, rect.height);
		XFlush(disp);
	}
	XCloseDisplay(disp);
	crop_coords.x = rect.x+1;
	crop_coords.y = rect.y+1;
	crop_coords.width = rect.width-1;
	crop_coords.height = rect.height-1;
	return crop_coords;
}
