#include "screenshot.h"


GdkRectangle resize_keep_aspect(GdkRectangle original, int size, enum resize_mode resize) {
	GdkRectangle resized;

	switch (resize) {
		case MODE_WIDTH:
			resized.width = size + thumb_zoom;
			resized.height = size * original.height/original.width + thumb_zoom;
			break;
		case MODE_HEIGHT:
			resized.height = size + thumb_zoom;
			resized.width = size * original.width/original.height + thumb_zoom;
			break;	
	}

	return resized;
}

GdkPixbuf* take_screenshot(GdkRectangle crop_coords) {
	GdkWindow *root = gdk_get_default_root_window();
	return gdk_pixbuf_get_from_window(root, 
		crop_coords.x,
		crop_coords.y,
		crop_coords.width,
		crop_coords.height
	);
}

GdkPixbuf* get_thumbnail(GdkPixbuf* screenshot) {
	// Get the image resolution
	GdkRectangle resolution = {
		0,0,
		gdk_pixbuf_get_width(screenshot),
		gdk_pixbuf_get_height(screenshot),
	};

	// Get the available space for the frame widget that contains the GtkImage
	GtkAllocation available;
	gtk_widget_get_allocation(widgets.frame, &available);
	// Remove padding
	available.width = available.width - available.x;
	available.height = available.height - available.y;

	// If no thumbs needed, no thumbs given
	if (resolution.height < available.height && resolution.width < available.width)
		return gdk_pixbuf_copy(screenshot);

	// Negotiate fill width or height
	GdkRectangle thumb_resolution = resize_keep_aspect(resolution, available.width, MODE_WIDTH);
	if (thumb_resolution.height > available.height)
		thumb_resolution = resize_keep_aspect(resolution, available.height, MODE_HEIGHT);

	return gdk_pixbuf_scale_simple(screenshot, thumb_resolution.width, thumb_resolution.height, INTERPOLATION);
}
