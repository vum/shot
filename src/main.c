#include <libgen.h>
#include <unistd.h>
#include <limits.h>

#include "globals.h"

#define PROGRAM_NAME "shot"
#define AUTHORS "Facundo Lander"


struct Widgets widgets;

int thumb_zoom;

void usage() {
	exit(1);
}

int main(int argc, char *argv[]) {
	char datapath[PATH_MAX], window_main_path[PATH_MAX], save_file_path[PATH_MAX];
	ssize_t size = readlink("/proc/self/exe", datapath, sizeof(datapath)-1);
	datapath[size] = '\0';
	dirname(datapath);
	strcat(strcat(datapath, "/"), DATAPATH);
	strcpy(window_main_path, datapath);
	strcpy(save_file_path, datapath);
	strcat(window_main_path, "window_main.glade");
	strcat(save_file_path, "save_file.glade");

	gtk_init(&argc, &argv);

	// Build Main Window
	GtkBuilder *builder   = gtk_builder_new_from_file(window_main_path);
	gtk_builder_connect_signals(builder, NULL);
	GtkWindow *window     = GTK_WINDOW (gtk_builder_get_object(builder, "window_main"));
	widgets.frame         = GTK_WIDGET (gtk_builder_get_object(builder, "frame"));
	widgets.scrot         = GTK_IMAGE  (gtk_builder_get_object(builder, "scrot"));
	widgets.button_upload = GTK_BUTTON (gtk_builder_get_object(builder, "upload"));
	widgets.button_save   = GTK_BUTTON (gtk_builder_get_object(builder, "save"));
	widgets.button_copy   = GTK_BUTTON (gtk_builder_get_object(builder, "copy"));

	// Build File Chooser
	builder = gtk_builder_new_from_file(window_main_path);
	gtk_builder_connect_signals(builder, NULL);
	widgets.save_file = GTK_FILE_CHOOSER_DIALOG (gtk_builder_get_object(builder, "save_file"));

	g_object_unref(builder);

	gtk_widget_show((GtkWidget *)window);
	gtk_main();

	return 0;
}
