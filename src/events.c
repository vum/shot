#include "globals.h"
#include "screenshot.h"
#include "crop.h"


// When the frame widget gets resized
void on_resize() {
	if (widgets.screenshot) {
		widgets.thumbnail = get_thumbnail(widgets.screenshot);
		gtk_image_set_from_pixbuf(widgets.scrot, widgets.thumbnail);
	}
}
// When mouse wheel is used on the frame widget
void on_frame_scrollwheel(gpointer _, GdkEventScroll *scroll) {

	return;
	// Now, how do I fix this mess? (too slow/sloppy)

	if (!scroll->delta_y || !widgets.screenshot) return;

	if (scroll->delta_y < 0) { // Up (->direction doesnt work?)
		thumb_zoom += ZOOM_POWER;
	} else {
		thumb_zoom -= ZOOM_POWER;
	}

	on_resize();
}

void on_pick_clicked() {
	GdkRectangle crop_coords = crop();

	if (crop_coords.width < 1 || crop_coords.height < 1)
		return;

	widgets.screenshot = take_screenshot(crop_coords);
	widgets.thumbnail = get_thumbnail(widgets.screenshot);

	gtk_image_set_from_pixbuf(widgets.scrot, widgets.thumbnail);
	gtk_widget_set_sensitive((GtkWidget *)widgets.button_upload, TRUE);
	gtk_widget_set_sensitive((GtkWidget *)widgets.button_save, TRUE);
	gtk_widget_set_sensitive((GtkWidget *)widgets.button_copy, TRUE);
}

void on_save_clicked() {
	if (gtk_dialog_run(GTK_DIALOG(widgets.save_file)) == GTK_RESPONSE_OK) {
		char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(widgets.save_file));
		printf("%s\n", filename);

		char *extension = strrchr(filename, '.'), *used_extension = "jpeg";
		const char *supported_extensions[4] = {"jpeg", "png", "ico", "bmp"};
		for (int i = 0; i < 4; ++i)
			if (extension == supported_extensions[i])
				used_extension = extension;

		// What's the difference between this and gdk_pixbuf_savev?
		gdk_pixbuf_save(widgets.screenshot, filename, used_extension, NULL, NULL);
		//g_free(filename);
	}
	gtk_widget_hide((GtkWidget *)widgets.save_file);
}

void on_copy_clicked() {
	GtkClipboard *paster = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
	gtk_clipboard_set_image(paster, widgets.screenshot);
	//notify("Image copied to clipboard!"); TODO: GtkRevealer or system notifications
}

// When program goes bye bye
void on_window_main_destroy() {
	gtk_main_quit();
}
