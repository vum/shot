# DEPS
#  + libgtk-3-dev

BINARY = shot
TARGET = bin/$(BINARY)
PREFIX = /usr/local
DATADIR = $(PREFIX)/share
BINDIR = $(PREFIX)/bin

# --------------------

GTKLIB = `pkg-config --cflags --libs gtk+-3.0`
DISPLAYSERVERLIB = -lX11 -lXfixes
CC = gcc
CCFLAGS = -Wall -O3 -pipe -Iinclude -s
LD = gcc
LDFLAGS = $(GTKLIB) $(DISPLAYSERVERLIB) -export-dynamic
OBJS = main.o events.o crop.o screenshot.o

TXT_DONE = [\033[32mDONE\033[0m]
C_GREEN = \033[32m
C_ORANGE = \033[33m
C_RED = \033[31m
C_RESET = \033[0m

# --------------------

.PHONY: all
all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)
	@printf "$(C_ORANGE)$(OBJS)$(C_RESET) -> $(C_GREEN)$(TARGET)$(C_RESET)\n"
	@printf "$(TXT_DONE) Compilation finalized!\n"

# --------------------

%.o: src/%.c
	$(CC) -c $(CCFLAGS) $(GTKLIB) $< -o $@
	@printf "$(C_ORANGE)$<$(C_RESET) -> $(C_GREEN)$@$(C_RESET)\n"

# --------------------

.PHONY: install
install: $(TARGET)
	install -Dm555 $(TARGET) $(DESTDIR)$(BINDIR)
	install -Dt $(DESTDIR)$(DATADIR)/$(BINARY) -m444 share/$(BINARY)/*.glade

.PHONY: uninstall
uninstall:
	rm $(DESTDIR)$(BINDIR)/$(BINARY)
	rm -r $(DESTDIR)$(DATADIR)/$(BINARY)

.PHONY: clean
clean:
	@for file in $(OBJS) $(TARGET); do \
		if [ -f $$file ]; then\
			rm $$file;\
			printf "<- $(C_RED)$$file$(C_RESET)\n";\
		fi;\
	done
	@#rm -f $(OBJS) $(TARGET) # I wanted the cool output!!!! ^^^^
	@printf "$(TXT_DONE) Removed all objects and the target!\n"
